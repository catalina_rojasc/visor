function initMap(){
    var mymap = L.map('visor',  { center:[4.625770, -74.172777],  zoom:16 });
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {        
            maxZoom: 20,
            id: 'mapbox.streets'
         }).addTo(mymap);

         var popup1 = L.popup()
         .setLatLng([4.656983, -74.093381])
         .setContent("Parque Simón Bolivar")
         .openOn(mymap);
        var marker = L.marker([4.656983, -74.093381]).addTo(mymap);
        marker.bindPopup("<b>Parque Simón Bolivar</b><br>").openPopup();
        var circle = L.circle([4.656983, -74.093381], {
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0.5,
            radius: 500
        }).addTo(mymap);

        var popup2 = L.popup()
         .setLatLng([4.663395, -74.0871501])
         .setContent("Parque Salitre")
         .openOn(mymap);
         var marker = L.marker([4.663395, -74.087150]).addTo(mymap);
         marker.bindPopup("<b>Parque Salitre</b><br>").openPopup();
         var circle = L.circle([4.663395, -74.087150], {
            color: 'blue',
            fillColor: 'blue',
            fillOpacity: 0.5,
            radius: 500
        }).addTo(mymap);

         var popup3 = L.popup()
         .setLatLng([4.597569, -74.081364])
         .setContent("Parque Tercer Milenio")
         .openOn(mymap);
         var circle = L.circle([4.597569, -74.081364], {
            color: 'green',
            fillColor: 'green',
            fillOpacity: 0.5,
            radius: 500
        }).addTo(mymap);

         var marker = L.marker([4.597569, -74.081364]).addTo(mymap);
         marker.bindPopup("<b>Parque Tercer Milenio</b><br>").openPopup();

         var popup4 = L.popup()
         .setLatLng([4.633885, -74.064761])
         .setContent("Panadería")
         .openOn(mymap);

         var marker = L.marker([4.633885, -74.064761]).addTo(mymap);
         marker.bindPopup("<b>Panadería Pan Dorado</b><br> Chapinero").openPopup();
         var circle = L.circle([4.633885, -74.064761], {
            color: 'yellow',
            fillColor: 'yellow',
            fillOpacity: 0.5,
            radius: 500
        }).addTo(mymap);

        var marker = L.marker([4.628134, -74.065530]).addTo(mymap);
        marker.bindPopup("<b>Universidad Distrital Francisco Jose de caldas</br> <br> Sede Ingeniería ").openPopup();
       



    





        }
        
        
